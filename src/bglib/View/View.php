<?php

namespace bglib\View;

use Psr\Http\Message\ResponseInterface;
use Interop\Container\ContainerInterface;

/**
 * View - Render PHP view scripts into a PSR-7 Response object
 */
class View
{

    const HEADER_TYPE_JS = 'js';

    const HEADER_TYPE_CSS = 'css';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var string
     */
    protected $templatePath;

    /**
     * @var string
     */
    protected $layoutFile;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * @var string
     */
    protected $content;

    /**
     * @var array
     */
    protected $header = [ self::HEADER_TYPE_CSS => [], self::HEADER_TYPE_JS => [] ];

    /**
     *
     * @var array
     */
    protected $appendHeader = [ self::HEADER_TYPE_CSS => [], self::HEADER_TYPE_JS => [] ];

    /**
     *
     * @var string
     */
    protected $template;

    /**
     *
     * @var boolean
     */
    protected $terminal = false;

    /**
     *
     * @var boolean
     */
    protected $rendered = false;

    /**
     *
     * @var array
     */
    protected $mca = array();

    /**
     * SlimRenderer constructor.
     *
     * @param ContainerInterface $container DI Container
     * @param string $templatePath
     * @param string $layoutFile
     * @param array $attributes [optional]
     */
    public function __construct(ContainerInterface $container, $templatePath, $layoutFile, $attributes = [])
    {
        $this->container = $container;
        $this->templatePath = $templatePath;
        $this->layoutFile = $layoutFile;
        $this->assign($attributes);
    }

    /**
     * Returns escaped value
     *
     * @param string $value Value
     * @param string $encoding [optional] Encoding
     * @return string
     */
    public function escape($value, $encoding = "UTF-8")
    {
        return \htmlentities($value, ENT_QUOTES | ENT_SUBSTITUTE, $encoding);
    }

    /**
     * Fügt einen Header hinzu
     *
     * @param string $type
     * @param string $filename
     * @param boolean $mtime [optional]
     */
    public function addHeader($type, $filename, $mtime = true)
    {
        $this->header[$type][] = array(
            'filename' => $filename,
            'mtime' => $mtime
        );
    }

    /**
     * Fügt einen Header an das Ende an
     *
     * @param string $type
     * @param string $filename
     * @param boolean $mtime [optional]
     * @param integer $offset [optional]
     * @throws \Exception
     */
    public function appendHeader($type, $filename, $mtime = true, $offset = null)
    {
        $values = array(
            'filename' => $filename,
            'mtime' => $mtime
        );

        if (!$offset) {
            $this->appendHeader[$type][] = $values;
        } else {
            if (isset($this->appendHeader[$type][$offset]) || isset($this->header[$type][$offset])) {
                throw new \Exception('Header offset ' . $offset . ' exists!');
            }

            $this->appendHeader[$type][$offset] = $values;
        }
    }

    /**
     * Gibt die Header zurück
     *
     * @param string $type
     * @return array
     */
    public function getHeader($type)
    {
        ksort($this->appendHeader[$type]);
        return $this->header[$type] + $this->appendHeader[$type];
    }

    /**
     * Render a template
     *
     * $data cannot contain template as a key
     *
     * throws RuntimeException if $templatePath . $template does not exist
     *
     * @param ResponseInterface $response
     * @param array $data
     *
     * @return ResponseInterface
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function render(ResponseInterface $response, array $data = [])
    {
        $this->setProperties($data);

        if (!$this->template) {
            $this->setTemplate($this->getTemplateByCalledUri());
        }

        $this->content = $this->fetch($this->template);

        if (!$this->terminal) {
            $output = $this->fetch($this->layoutFile);
            $response->getBody()->write($output);
        } else {
            $response->getBody()->write($this->content);
        }

        $this->rendered = true;

        return $response;
    }

    /**
     * Set the template
     *
     * @param string $template
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * If terminal true the default layout will not be used
     *
     * @param boolean $terminal
     * @return $this
     */
    public function setTerminal($terminal)
    {
        $this->terminal = $terminal;
        return $this;
    }

    /**
     * Get an array with the module, controller, action by requested uri
     *
     * @return array
     */
    public function getModuleControllerAction()
    {
        if (empty($this->mca)) {
            $url = parse_url($this->container->get('request')->getUri(), PHP_URL_PATH);
            $parts = explode('/', $url);


            $module = 'Application';
            $controller = (isset($parts[2]) && $parts[2] != '') ? $parts[2] : 'index';
            $action = (isset($parts[3]) && $parts[3] != '') ? $parts[3] : 'index';

            if (isset($parts[1]) && $parts[1] != '') {
                $module = '';
                foreach (explode('-', $parts[1]) as $prop => $value) {
                    $module .= ucfirst($value);
                }
            }

            $this->mca = array(
                'module' => $module,
                'controller' => $controller,
                'action' => $action
            );
        }

        return $this->mca;
    }

    /**
     * Get template path by requested uri
     *
     * @return string
     */
    public function getTemplateByCalledUri()
    {
        $mca = $this->getModuleControllerAction();

        $template = 'modules/' . $mca['module'] . '/views/' . $mca['controller'] . '/' . $mca['action'] . '.phtml';

        return $template;
    }

    /**
     * Get the attributes for the renderer
     *
     * @return array
     */
    public function getAssignedValues()
    {
        return $this->attributes;
    }

    /**
     * Set the attributes for the renderer
     *
     * @param array $attributes
     */
    public function assign(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->attributes[$key] = $value;
        }
    }

    /**
     * Get the layout file path
     *
     * @return string
     */
    public function getLayoutFilePath()
    {
        return $this->layoutFile;
    }

    /**
     * Checks view is rendered
     *
     * @return boolean
     */
    public function isRendered()
    {
        return $this->rendered;
    }

    /**
     * Checks is terminal mode
     *
     * @return boolean
     */
    public function isTerminal()
    {
        return $this->terminal;
    }

    /**
     * Get the template path
     *
     * @return string
     */
    public function getTemplatePath()
    {
        return $this->templatePath;
    }

    /**
     * Set the template path
     *
     * @param string $templatePath
     */
    public function setTemplatePath($templatePath)
    {
        $this->templatePath = $templatePath;
    }

    /**
     * Gibt den DI Container zurück
     *
     * @return \Interop\Container\ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Gibt den Content zurück
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Renders a template and returns the result as a string
     *
     * cannot contain template as a key
     *
     * throws RuntimeException if $templatePath . $template does not exist
     *
     * @param $template
     *
     * @return mixed
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function fetch($template)
    {
        $templatePath = $this->templatePath . '/' . $template;

        if (!is_file($templatePath)) {
            throw new \RuntimeException("View cannot render `$templatePath` because the template does not exist");
        }

        ob_start();
        $this->protectedIncludeScope($templatePath);
        $output = ob_get_clean();
        return $output;
    }

    /**
     * @param string $template
     */
    protected function protectedIncludeScope($template)
    {
        include $template;
    }

    /**
     * Set properties to use in the view
     *
     * @param array $data
     * @throws \InvalidArgumentException
     */
    protected function setProperties(array $data)
    {
        if (isset($data['template'])) {
            throw new \InvalidArgumentException("Duplicate template key found");
        }

        foreach (array_merge($this->attributes, $data) as $key => $value) {
            if (property_exists($this, $key)) {
                throw new \InvalidArgumentException("Duplicate class property. Do not use already declared class properties in data array");
            }

            $this->{$key} = $value;
        }
    }

    /**
     * Gibt einen View Helper zurück
     *
     * @param string $class Helper Klasse
     * @return object
     */
    public function helper($class)
    {
        return $this->container->get($class);
    }

    /**
     * Rendert eine view und gibt die Ausgabe zurück
     *
     * @param string $view View Skript
     * @param array $data View Daten
     * @return string
     */
    public function partial($view, array $data = [])
    {
        ob_start();
        extract($data);
        include 'app/helper/view/' . $view;
        $output = ob_get_clean();
        return $output;
    }

}
