<?php

namespace bglib\Cache;

use Psr\Cache\CacheItemPoolInterface;

/**
 * PSR-6 CacheItemPool Implementation for Memcached
 */
class CacheItemPool implements CacheItemPoolInterface
{

    /**
     * @var \Memcached
     */
    private $cache;

    public function __construct(\Memcached $cache)
    {
        $this->cache = $cache;
    }

    public function clear()
    {
        $keys = $this->cache->getAllKeys();
        if ($keys)
        {
            foreach ($keys as $key)
            {
                $this->cache->delete($key);
            }
            return true;
        }
        return false;
    }

    public function commit()
    {
        throw new \Exception('not yet implemented');
    }

    public function deleteItem($key)
    {
        return $this->cache->delete($key);
    }

    public function deleteItems(array $keys)
    {
        return $this->cache->deleteMulti($keys);
    }

    public function getItem($key)
    {
        $value = $this->cache->get($key);
        return new CacheItem($key, $value);
    }

    public function getItems(array $keys = array())
    {
        $result = [];
        foreach ($keys as $key)
        {
            $value = $this->cache->get($key);
            if ($value)
            {
                $result[] = new CacheItem($key, $value);
            }
        }
        return $result;
    }

    public function getAllKeys()
    {
        return $keys = $this->cache->getAllKeys();
    }

    public function hasItem($key)
    {
        if (false === $this->cache->get($key))
        {
            return false;
        }

        return true;
    }

    public function save(\Psr\Cache\CacheItemInterface $item)
    {
        return $this->cache->set($item->getKey(), $item->get(), $item->expiration);
    }

    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
        throw new \Exception('not yet implemented');
    }

}
