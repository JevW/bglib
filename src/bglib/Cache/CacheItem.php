<?php

namespace bglib\Cache;

use Psr\Cache\CacheItemInterface;

/**
 * PSR-6 CacheItem Implementation for Memcached
 */
class CacheItem implements CacheItemInterface
{

    private $key;

    private $value;

    private $time;

    public $expiration;

    public function __construct($key, $value, $expiration = null)
    {
        $this->key = $key;
        $this->value = $value;
        $this->expiration = $expiration;
    }

    public function getExpiration()
    {
        return $this->expiration;
    }

    public function expiresAfter($time)
    {
        $this->time = $time;
    }

    public function expiresAt($expiration)
    {
        $this->expiration = $expiration;
    }

    public function get()
    {
        return $this->value;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function isHit()
    {
        throw new \Exception('not yet implemented');
    }

    public function set($value)
    {
        $this->value = $value;
    }

}
