<?php

namespace bglib\Money;

/**
 * A simple Money Class for PHP
 */
class Money implements \JsonSerializable
{
    /**
     * @var int
     */
    private $amount;

    /**
     * @var int
     */
    private $baseUnit;

    /**
     * @var string
     */
    private $currency;

    /**
     * Retruns the calculated amount
     *
     * @return float Calculated amount
     */
    private function calculateAmount(): float
    {
        return $this->amount / $this->baseUnit;
    }

    /**
     * Currency constructor.
     *
     * @param int|float $amount Amount
     * @param string $currency Currency-Name
     * @param int $baseUnit Base-Unit
     */
    public function __construct($amount = 0, string $currency = 'EUR', int $baseUnit = 100)
    {
        $this->_validateBaseUnit($baseUnit);

        $normalizedAmount = $this->_normalizeDecimal($amount, $baseUnit);

        $this->amount = (int)($normalizedAmount * $baseUnit);
        $this->baseUnit = $baseUnit;
        $this->currency = $currency;
    }


    /**
     * Validates the given baseUnit
     *
     * @param int $baseUnit
     */
    private function _validateBaseUnit(int $baseUnit)
    {
        foreach (str_split((string)$baseUnit) as $prop => $value) {
            if ($prop === 0) {
                if ($value !== '1') {
                    throw new \InvalidArgumentException('BaseUnit is not leading by one');
                }
            } else {
                if ($value !== '0') {
                    throw new \InvalidArgumentException('BaseUnit is not following by zero. Allowed values are 1, 10, 100, 1000...');
                }
            }
        }
    }

    /**
     * Normalizes a given decimal value
     *
     * @param $val
     * @param int $baseUnit
     * @return string
     */
    private function _normalizeDecimal($val, int $baseUnit): string
    {
        $precision = strlen((string)$baseUnit) - 1;

        $input = str_replace(' ', '', $val);
        $number = str_replace(',', '.', $input);
        if (strpos($number, '.')) {
            $groups = explode('.', str_replace(',', '.', $number));
            $lastGroup = array_pop($groups);
            $number = implode('', $groups) . '.' . $lastGroup;
        }

        return number_format($number, $precision, '.', '');
    }

    /**
     * Returns the Raw Amount
     *
     * @return int
     */
    public function getRawAmount(): int
    {
        return $this->amount;
    }

    /**
     * Returns the Amount divided by the Base-Unit
     *
     * @var int $decimals Decimals
     * @return int|float
     */
    public function getAmount(int $decimals = 2): float
    {
        $amount = $this->calculateAmount();
        if ($pos = strpos($amount, '.')) {
            return substr($amount, 0, $pos + 1 + $decimals);
        }

        return $amount;
    }

    public function getFormatedAmount($decimals = 2, $decPoint = ',', $thousandsSep = ''): string
    {
        $amount = $this->calculateAmount();
        return number_format($amount, $decimals, $decPoint, $thousandsSep);
    }

    /**
     * Returns the Base-Unit
     *
     * @return int
     */
    public function getBaseUnit(): int
    {
        return $this->baseUnit;
    }

    /**
     * Returns the Currency
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * Addition
     *
     * @param Money $money Money-Object
     * @return $this
     */
    public function add(Money $money)
    {
        $this->amount = $this->amount + $money->getRawAmount();

        return $this;
    }

    /**
     * Subtraction
     *
     * @param Money $money
     * @return $this
     */
    public function subtract(Money $money)
    {
        $this->amount = $this->amount - $money->getRawAmount();

        return $this;
    }

    /**
     * @param int|float $multiplier
     * @return $this
     */
    public function multiply($multiplier)
    {
        $this->amount = $this->amount * $multiplier;

        return $this;
    }

    /**
     * @param  int|float $divisor
     * @return $this
     */
    public function divide($divisor)
    {
        $this->amount = $this->amount / $divisor;

        return $this;
    }

    /**
     * __toString
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getFormatedAmount();
    }

    /**
     * __clone
     *
     * @return Money
     */
    public function __clone()
    {
        return new Money($this->getRawAmount(), $this->currency, $this->baseUnit);
    }

    /**
     * Return Array
     *
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return array|mixed data which can be serialized by <b>json_encode</b>
     */
    public function jsonSerialize()
    {
        return [
            'amount' => $this->amount,
            'baseUnit' => $this->baseUnit,
            'currency' => $this->currency,
        ];
    }
}