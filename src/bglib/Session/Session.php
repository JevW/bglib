<?php

namespace bglib\Session;

/**
 * Session
 */
class Session
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $lifetime;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var boolean
     */
    private $ssl;

    /**
     *
     * @var boolean
     */
    private $started = false;

    /**
     * Set some options for security
     */
    private function init()
    {
        ini_set("session.hash_function", 1); // Hash Funktion von MD5 auf SHA256 setzen
        ini_set("session.use_trans_sid", 0); // Session ID darf nur im Cookie gespeichert werden
        ini_set("session.cookie_httponly", 1); // Auslesen mittels Javascript verhindern
        ini_set("session.use_only_cookies", 1); // Nur Cookies benutzen
    }

    /**
     * Konstruktor
     * @param string $name Session name
     * @param int $lifetime Cookie Liefetime
     * @param string $path Cookie Path
     * @param string $domain Cookie Domain
     * @param boolean $ssl [optional] https true or not
     */
    public function __construct($name = 'appname', $lifetime = 7200, $path = "/", $domain = "", $ssl = false)
    {
        $this->init();
        $this->name = $name;
        $this->lifetime = $lifetime;
        $this->path = $path;
        $this->domain = $domain;
        $this->ssl = $ssl;
    }

    /**
     * Destroy Session
     */
    public function destroy()
    {
        setcookie($this->name, '', 1, $this->path, $this->domain, $this->ssl, true);
        session_destroy();
        unset($_SESSION);
    }

    /**
     * Returns the Session ID
     * @return string
     */
    public function getId()
    {
        return session_id();
    }

    /**
     * Regenerates the Session ID
     */
    public function newId()
    {
        session_regenerate_id(false);
    }

    /**
     * Returns Session Data
     * @return array
     */
    public function read()
    {
        return $_SESSION;
    }

    /**
     * Starts a new Session
     */
    public function start()
    {
        if (false === $this->started)
        {
            session_set_cookie_params($this->lifetime, $this->path, $this->domain, $this->ssl, true);
            session_name($this->name);
            session_start();
            $this->started = true;
        }
    }

    /**
     * Writes Session Data
     * @param array $data
     */
    public function write(array $data)
    {
        $_SESSION = $data;
    }

    /**
     * Returns TRUE if Session is started and FALSE if is not
     * @return boolean
     */
    public function isStarted()
    {
        return $this->started;
    }

}
